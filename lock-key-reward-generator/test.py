#!/usr/bin/python3


from solver import NORMKEY
from solver import STKEY
from solver import SPKEY
from solver import ITEMKEY
from solver import MULTKEY
from solver import LOOPKEY
from solver import BOSSKEY
from solver import STARTKEY

from solver import NORMLOCK
from solver import STLOCK
from solver import SPLOCK
from solver import ITEMLOCK
from solver import MULTLOCK
from solver import LOOPLOCK
from solver import BOSSLOCK
from solver import STARTLOCK
from solver import DUNGEON

from generator import generateRandomDungeon
from mapprinter import graphDungeon

DKEY = 'DKEY'
D2KEY = 'D2KEY'
LOOPKEY = 'c'
MULTIKEY = 'M'
STATEKEY = 'STATE'

print('--------- Random dungeon ----------')
dungeon = DUNGEON([], [BOSSKEY(), BOSSLOCK('END')])

normalkeymax = 3
specialkeymax = 1
dungeonitemmax = 1
dungeon2itemmax = 0
multiplekeymax = 0
statekeymax = 0
loopmax = 0

dungeon = generateRandomDungeon(dungeon,
                                normalkeymax,
                                specialkeymax,
                                dungeonitemmax,
                                dungeon2itemmax,
                                multiplekeymax,
                                statekeymax,
                                loopmax)
graphDungeon(dungeon, 'generateddungeon', 'png')
dungeon.solveDungeonAll()

print('----------Wind Waker: Dragonroost------------')
dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
                                            NORMLOCK([NORMKEY(),
                                                      NORMLOCK([NORMKEY(),
                                                                NORMLOCK([NORMKEY(),
                                                                          NORMLOCK([ITEMKEY(DKEY),
                                                                                    BOSSKEY(),
                                                                                    ITEMLOCK(DKEY, BOSSLOCK('Exit'))])])])])])])
graphDungeon(dungeon, 'dragonroost', 'png')
dungeon.solveDungeonAll()


print('----------Wind Waker: Forbidden Woods------------')
dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
                                            NORMLOCK([ITEMKEY(DKEY),
                                                           ITEMLOCK(DKEY, BOSSKEY())]),
                                            ITEMLOCK(DKEY, BOSSLOCK('Exit'))])])
graphDungeon(dungeon, 'forbiddenwoods', 'png')
dungeon.solveDungeonAll()


print('----------Wind Waker: Tower of the gods------------')
dungeon = DUNGEON([STARTKEY()], [STARTLOCK([SPKEY('A'),
                                            SPLOCK('A', NORMKEY()),
                                            NORMLOCK(SPKEY('B')),
                                            SPLOCK('B', [SPKEY('C'),
                                                         SPLOCK('C', [SPKEY('D'),
                                                                      SPLOCK('D', ITEMKEY(DKEY)),
                                                                      ITEMLOCK(DKEY, SPKEY('E'))]),
                                                         SPLOCK('E', [NORMKEY(),
                                                                      NORMLOCK(SPKEY('F'))]),
                                                         SPLOCK('F', [BOSSKEY(),
                                                                      BOSSLOCK('Exit')])])])])
graphDungeon(dungeon, 'towerofthegods', 'png')
dungeon.solveDungeonAll()


print('----------Wind Waker: Wind Temple------------')
dungeon = DUNGEON([STARTKEY()], [STARTLOCK([SPLOCK('C', NORMKEY()),
                                            ITEMLOCK(DKEY, BOSSKEY()),
                                            ITEMLOCK(DKEY, SPKEY('B')),
                                            SPLOCK('D', ITEMKEY(DKEY)),
                                            SPLOCK('A', [ITEMLOCK(DKEY, SPKEY('C')),
                                                         SPLOCK('B', NORMLOCK(BOSSLOCK('Exit'))),
                                                         NORMLOCK(SPKEY('D')),
                                                         NORMKEY()]),
                                                         SPKEY('A')])])
graphDungeon(dungeon, 'windtemple', 'png')
dungeon.solveDungeonAll()

#print('----------Wind Waker: Earth Temple------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
#                                            NORMLOCK([NORMKEY(),
#                                                      NORMLOCK(ITEMKEY(DKEY))]),
#                                            ITEMLOCK(DKEY, [SPKEY('A'),
#                                                            NORMKEY(),
#                                                            NORMLOCK(SPLOCK('A', [BOSSKEY(),
#                                                                                  BOSSLOCK('Exit')]))])])])
#graphDungeon(dungeon, 'earthtemple', 'png')
#
#
#print('----------ALTTP: Desert Palace------------')
#dungeon = DUNGEON([STARTKEY(), SPKEY('A')], [STARTLOCK([NORMKEY(),
#                                                        NORMLOCK(BOSSKEY()),
#                                                        ITEMLOCK(DKEY, [NORMKEY(),
#                                                                        NORMLOCK([NORMKEY(),
#                                                                                  NORMLOCK([NORMKEY(),
#                                                                                            NORMLOCK(BOSSLOCK('Exit'))])])]),
#                                                        SPLOCK('A', ITEMKEY(DKEY))])])
#graphDungeon(dungeon, 'desertpalace', 'png')
#
#print('----------ALTTP: Tower of Hera------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
#                                 NORMLOCK(BOSSKEY()), BOSSLOCK('Exit')])])
#graphDungeon(dungeon, 'towerofhera', 'png')
#
#
#print('----------ALTTP: Hyrule Castle 2------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
#                                            NORMLOCK([NORMKEY(),
#                                                      NORMLOCK([NORMKEY(),
#                                                                NORMLOCK([NORMKEY(),
#                                                                          NORMLOCK('Exit')])])])])])
#graphDungeon(dungeon, 'hyrulecastle2', 'png')
#
#print('----------ALTTP: Darkness------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
#                                            NORMKEY(),
#                                            NORMLOCK([NORMKEY(),
#                                                      NORMKEY(),
#                                                      NORMLOCK(BOSSKEY()),
#                                                      NORMLOCK([NORMKEY(),
#                                                                NORMLOCK([BOSSLOCK(ITEMKEY(DKEY)),
#                                                                          NORMKEY()]),
#                                                                ITEMLOCK(DKEY, NORMLOCK('Exit'))])])])])
#graphDungeon(dungeon, 'darkness', 'png')
#
#
#print('----------ALTTP: Swamp Palace------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
#                                            NORMLOCK([NORMKEY(),
#                                                      NORMLOCK([NORMKEY(),
#                                                                NORMLOCK(SPKEY('A')),
#                                                                SPLOCK('A', [NORMKEY(),
#                                                                             SPLOCK('B', BOSSKEY()),
#                                                                             BOSSLOCK(ITEMKEY(DKEY)),
#                                                                             ITEMLOCK(DKEY, NORMKEY()),
#                                                                             NORMLOCK(SPKEY('B')),
#                                                                             ITEMLOCK(DKEY, NORMLOCK([NORMKEY(),
#                                                                                                      NORMLOCK('Exit')]))])])])])])
#graphDungeon(dungeon, 'swamppalace', 'png')
#
#print('----------ALTTP: Turtle Rock------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
#                                            NORMLOCK([NORMKEY(),
#                                                      NORMLOCK([NORMKEY(),
#                                                                NORMLOCK([NORMKEY(),
#                                                                          NORMLOCK([BOSSKEY(),
#                                                                                    BOSSLOCK([ITEMKEY(DKEY),
#                                                                                              ITEMLOCK(DKEY, [NORMKEY(),
#                                                                                                              NORMLOCK([NORMKEY(),
#                                                                                                                        NORMLOCK('Exit')])])])])])])])])])
#graphDungeon(dungeon, 'turtlerock', 'png')
#
#print('----------Links Awakening: Face Shrine ------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
#                                            ITEMKEY(DKEY),
#                                            ITEMLOCK(DKEY, NORMKEY()),
#                                            NORMLOCK(ITEMLOCK(DKEY, [NORMKEY(),
#                                                                     NORMLOCK(NORMLOCK(BOSSKEY())),
#                                                                     BOSSLOCK('Exit')]))])])
#graphDungeon(dungeon, 'faceshrine', 'png')
#
#print('----------Ocarina of time: Fire Temple------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([NORMKEY(),
#                                            NORMLOCK([NORMKEY(),
#                                                      NORMKEY(),
#                                                      NORMLOCK(NORMLOCK([NORMKEY(),
#                                                                         NORMKEY(),
#                                                                         NORMLOCK(NORMLOCK([NORMKEY(),
#                                                                                            NORMKEY(),
#                                                                                            NORMLOCK([ITEMLOCK(DKEY, BOSSLOCK('Exit')),
#                                                                                                      NORMLOCK([ITEMKEY(DKEY),
#                                                                                                                NORMKEY()])])]))]))]),
#                                            ITEMLOCK(DKEY, NORMLOCK(BOSSKEY()))])])
#graphDungeon(dungeon, 'firetemple', 'png')
#
#
#print('----------Ocarina of time: Shadow Temple------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([ITEMKEY(DKEY),
#                                            ITEMLOCK(DKEY, [NORMKEY(),
#                                                            NORMLOCK([NORMKEY(),
#                                                                      NORMLOCK([NORMKEY(),
#                                                                                NORMLOCK([NORMKEY(),
#                                                                                          NORMLOCK([NORMKEY(),
#                                                                                                    BOSSKEY(),
#                                                                                                   NORMLOCK(BOSSLOCK('Exit'))])])])])])])])
#graphDungeon(dungeon, 'shadowtemple', 'png')
#
#
#print('----------Ocarina of time: Water Temple------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([STKEY('1'),
#                                            STLOCK('1', [NORMKEY(),
#                                                         NORMKEY(),
#                                                         NORMLOCK(STKEY('2')),
#                                                         ITEMLOCK(DKEY, NORMLOCK(NORMLOCK(BOSSKEY())))]),
#                                            STLOCK('2', [NORMKEY(),
#                                                         NORMKEY(),
#                                                         NORMLOCK(STKEY('3')),
#                                                         ITEMLOCK(DKEY, NORMKEY())]),
#                                            STLOCK('3', [NORMLOCK(NORMLOCK([ITEMKEY(DKEY),
#                                                                            NORMKEY()])),
#                                                         ITEMLOCK(DKEY, BOSSLOCK('Exit'))])])])
#graphDungeon(dungeon, 'watertemple', 'png')
#
#
#print('----------Majora''s Mask: Stone Tower Temple------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([SPLOCK('A', [NORMKEY(),
#                                                         NORMKEY(),
#                                                         SPLOCK('B', NORMLOCK(BOSSLOCK('R'))),
#                                                         SPKEY('B'),
#                                                         NORMLOCK(BOSSKEY())]),
#                                            ITEMLOCK(DKEY, SPKEY('A')),
#                                            NORMLOCK([NORMKEY(),
#                                                      NORMLOCK(ITEMKEY(DKEY))]),
#                                            NORMKEY()])])
#graphDungeon(dungeon, 'stonetower', 'png')
#
#
#print('----------Majora''s Mask: Great Bay Temple------------')
#dungeon = DUNGEON([STARTKEY()], [STARTLOCK([SPLOCK('A', SPLOCK('B', SPKEY('C'))),
#                                            SPLOCK('C', [MULTKEY(MULTIKEY),
#                                                         MULTKEY(MULTIKEY),
#                                                         MULTLOCK([MULTIKEY, MULTIKEY, MULTIKEY], BOSSLOCK('R'))]),
#                                            NORMLOCK(ITEMKEY(DKEY)),
#                                            ITEMLOCK(DKEY, SPKEY('A')),
#                                            ITEMLOCK(DKEY, SPKEY('B')),
#                                            ITEMLOCK(DKEY, BOSSKEY()),
#                                            ITEMLOCK(DKEY, MULTKEY(MULTIKEY)),
#                                            NORMKEY()])])
#graphDungeon(dungeon, 'greatbay', 'png')
#
#
#print('----------Twilight Princess: Forest Temple------------')
#dungeon = DUNGEON([STARTKEY(), MULTKEY(MULTIKEY, UNLIMITED)], [STARTLOCK([MULTLOCK([MULTIKEY, MULTIKEY], [NORMKEY(),
#                                                                                                          NORMKEY(),
#                                                                                                          NORMLOCK(MULTKEY(MULTIKEY, UNLIMITED)),
#                                                                                                          NORMLOCK(MULTKEY(MULTIKEY, UNLIMITED))]),
#                                                                          ITEMLOCK(DKEY, BOSSKEY()),
#                                                                          MULTLOCK([MULTIKEY, MULTIKEY, MULTIKEY, MULTIKEY], [ITEMKEY(DKEY),
#                                                                                                                              ITEMLOCK(DKEY, [MULTKEY(MULTIKEY, UNLIMITED),
#                                                                                                                                              LOOPKEY('C')])],'C'),
#                                                                          NORMKEY(),
#                                                                          NORMLOCK(MULTKEY(MULTIKEY, UNLIMITED)),
#                                                                          ITEMLOCK(DKEY, [NORMKEY(),
#                                                                                          MULTLOCK([MULTIKEY, MULTIKEY, MULTIKEY, MULTIKEY, MULTIKEY, MULTIKEY, MULTIKEY, MULTIKEY], BOSSLOCK('Exit')),
#                                                                                          NORMLOCK(MULTKEY(MULTIKEY, UNLIMITED)),
#                                                                                          MULTKEY(MULTIKEY, UNLIMITED),
#                                                                                          MULTKEY(MULTIKEY, UNLIMITED)])])])
#graphDungeon(dungeon, 'foresttemple', 'png')
