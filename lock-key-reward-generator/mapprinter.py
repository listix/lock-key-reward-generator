#!/usr/bin/python3

import drawSvg as draw
import math
import copy
from functools import cmp_to_key
from functools import partial

from solver import KEY
from solver import NORMKEY
from solver import STKEY
from solver import SPKEY
from solver import ITEMKEY
from solver import ITEMKEY2
from solver import MULTKEY
from solver import LOOPKEY
from solver import BOSSKEY
from solver import STARTKEY

from solver import LOCK
from solver import NORMLOCK
from solver import STLOCK
from solver import SPLOCK
from solver import ITEMLOCK
from solver import ITEMLOCK2
from solver import MULTLOCK
from solver import LOOPLOCK
from solver import BOSSLOCK
from solver import STARTLOCK

from solver import DUNGEON
from solver import UNLIMITED
from solver import ONETIME
from solver import recursiveSort
from solver import getDepth
from solver import flatten

NKEY = 'k'
SPECIALKEY = 'SP'
DKEY = 'DKEY'
D2KEY = 'D2KEY'
MULTIKEY = 'M'
STATEKEY = 'STATE'

COLORS = ['#1ade0b', '#de0bcd', '#09c2de', '#4e07de', '#dea203', '#de056c',
          '#0ade7a', '#0842de', '#ce06de', '#de2004', '#96de02', '#1ade0b',
          '#de0bcd', '#09c2de', '#4e07de', '#dea203', '#de056c']
ALPHABETSTART = 65
NUMBERSTART = 48


def createCanvas(size):
    width, height = size
    return draw.Drawing(width,
                        height,
                        origin=(0, -height),
                        displayInline=False)


def createSquare(canvas, length, position, fill):
    x, y = position
    canvas.append(draw.Rectangle(x, -y, x+length, (-y-length), fill=fill))


def createRectangle(canvas, position, size, fill):
    x, y = position
    width, height = size
    canvas.append(draw.Rectangle(x, -y, width, -(height), fill=fill))


def createCircle(canvas, position, radius, anglestart, angleend, stroke, fill):
    x, y = position
    canvas.append(draw.ArcLine(x, -y, radius, anglestart, angleend,
                               stroke=stroke, stroke_width=2, fill=fill,
                               fill_opacity=1))


def createLine(canvas, position1, position2, color, width):
    x1, y1 = position1
    x2, y2 = position2
    canvas.append(draw.Line(x1, -y1, x2, -y2, stroke=color,
                            stroke_width=width))


def createRoundedRectangle(canvas, position, size, radius, background):
    x, y = position
    width, height = size
    assert(2*radius <= width or 2*radius <= height)
    createRectangle(canvas, (x+radius, y), (width-2*radius, height),
                    background)
    createRectangle(canvas, (x, y+radius), (width, height-2*radius),
                    background)

    createCircle(canvas, (x+width-radius, y+radius), radius, 0, 90,
                 'black', background)
    createCircle(canvas, (x+radius, y+radius), radius, 90, 180,
                 'black', background)
    createCircle(canvas, (x+radius, y+height-radius), radius, 180, 270,
                 'black', background)
    createCircle(canvas, (x+width-radius, y+height-radius), radius, 270, 360,
                 'black', background)

    createLine(canvas, (x+radius, y), (x+width-radius, y), 'black', 2)
    createLine(canvas, (x+width, y+radius), (x+width, y+height-radius),
               'black', 2)
    createLine(canvas, (x, y+radius), (x, y+height-radius), 'black', 2)
    createLine(canvas, (x+radius, y+height), (x+width-radius, y+height),
               'black', 2)


def createRotatedRoundedRectangle(canvas, position, size, angle, radius,
                                  background):
    x, y = position
    width, height = size
    rotation = math.pi*angle/180
    assert(2*radius <= width or 2*radius <= height)
    cosrot = math.sin(rotation)
    sinrot = math.cos(rotation)
    canvas.append(
        draw.Lines(x-radius*cosrot,
                   -(y+radius*sinrot),

                   x-(height-radius)*cosrot,
                   -(y+(height-radius)*sinrot),

                   x-height*cosrot+radius*sinrot,
                   -(y+height*sinrot+radius*cosrot),

                   x-height*cosrot+(width-radius)*sinrot,
                   -(y+height*sinrot+(width-radius)*cosrot),

                   x+width*sinrot-(height-radius)*cosrot,
                   -(y+width*cosrot+(height-radius)*sinrot),

                   x+width*sinrot-radius*cosrot,
                   -(y+width*cosrot+radius*sinrot),

                   x+(width-radius)*sinrot,
                   -(y+(width-radius)*cosrot),

                   x+radius*sinrot,
                   -(y+radius*cosrot),

                   fill=background))

    createCircle(canvas, (x+radius*math.sqrt(2)*math.cos(3*math.pi/4-rotation),
                 y+radius*math.sqrt(2)*math.sin(3*math.pi/4-rotation)),
                 radius, angle-1, angle+90+1, 'black', background)
    createCircle(canvas, (x-(height-radius)*cosrot+radius*sinrot,
                 y+(height-radius)*sinrot+radius*cosrot),
                 radius, angle+90-1, angle+180+1, 'black', background)
    createCircle(canvas, (x+(width-radius)*sinrot-(height-radius)*cosrot,
                 y+(height-radius)*sinrot+(width-radius)*cosrot),
                 radius, angle+180-1, angle+270+1, 'black', background)
    createCircle(canvas, (x+(width-radius)*sinrot-radius*cosrot,
                 y+(width-radius)*cosrot+radius*sinrot),
                 radius, angle+270-1, angle+1, 'black', background)

    createLine(canvas, (x-radius*cosrot, y+radius*sinrot),
               (x-(height-radius)*cosrot, y+(height-radius)*sinrot),
               'black', 2)
    createLine(canvas, (x-height*cosrot+radius*sinrot,
                        y+height*sinrot+radius*cosrot),
               (x-height*cosrot+(width-radius)*sinrot,
                y+height*sinrot+(width-radius)*cosrot),
               'black', 2)
    createLine(canvas, (x+width*sinrot-(height-radius)*cosrot,
                        y+width*cosrot+(height-radius)*sinrot),
               (x+width*sinrot-radius*cosrot, y+width*cosrot+radius*sinrot),
               'black', 2)
    createLine(canvas, (x+radius*sinrot, y+radius*cosrot),
               (x+(width-radius)*sinrot, y+(width-radius)*cosrot),
               'black', 2)


def createNormalLock(canvas, center, size, radius, color):
    width, height = size
    centerx, centery = center
    position = (centerx-width/2, centery-height/2)
    createRoundedRectangle(canvas, position, size, radius, color)
    drawKey(canvas, center, 'black')


def createNormalKey(canvas, center, size, radius, color):
    width, height = size
    width = width/math.sqrt(2)
    height = height/math.sqrt(2)
    centerx, centery = center
    rotation = 45*math.pi/180
    cosrot = math.cos(rotation)
    sinrot = math.sin(rotation)
    position = (centerx + height*cosrot/2-width*sinrot/2,
                centery-height*sinrot/2-width*cosrot/2)
    createRotatedRoundedRectangle(canvas, position, (width, height), 45,
                                  radius, color)
    drawKey(canvas, center, 'white')


def drawLetter(canvas, center, size, text, color):
    x, y = center
    canvas.append(draw.Text(text, size, x, -y+7, center=True, fill=color))


def createSpecialLock(canvas, center, size, radius, name, color):
    width, height = size
    centerx, centery = center
    position = (centerx-width/2, centery-height/2)
    createRoundedRectangle(canvas, position, size, radius, color)
    drawLetter(canvas, (centerx, centery+3), 40, name, 'black')


def createSpecialKey(canvas, center, size, radius, name, color):
    width, height = size
    # converting the side lengths to the diagonal lengths to keep them the
    # same size with the lock
    width = width/math.sqrt(2)
    height = height/math.sqrt(2)
    centerx, centery = center
    # 45 degrees in radians
    rotation = 45*math.pi/180
    cosrot = math.cos(rotation)
    sinrot = math.sin(rotation)
    position = (centerx + height*cosrot/2-width*sinrot/2,
                centery-height*sinrot/2-width*cosrot/2)
    createRotatedRoundedRectangle(canvas, position, (width, height), 45,
                                  radius, color)
    drawLetter(canvas, (centerx, centery-2), 40, name, 'white')


def createStateLock(canvas, center, size, radius, name, color):
    width, height = size
    centerx, centery = center
    position = (centerx-width/2, centery-height/2)
    createRoundedRectangle(canvas, position, size, radius, color)
    smallerwidth = width * 0.8
    smallerheight = height * 0.8
    smallerposition = (centerx-smallerwidth/2, centery-smallerheight/2)
    createRoundedRectangle(canvas, smallerposition,
                           (smallerwidth, smallerwidth), radius, color)
    drawLetter(canvas, (centerx, centery+3), 40, name, 'black')


def createMultiLock(canvas, center, size, radius, namelist, color):
    width, height = size
    centerx, centery = center
    position = (centerx-width/2, centery-height/2)
    createRoundedRectangle(canvas, position, size, radius, color)
    drawLetter(canvas, (centerx, centery+3), 30,
               str(len(namelist)) + namelist[0], 'black')


def createMultiKey(canvas, center, size, radius, namelist, color):
    createSpecialKey(canvas, center, size, radius, namelist, color)


def createStateKey(canvas, center, size, radius, name, color):
    width, height = size
    width = width/math.sqrt(2)
    height = height/math.sqrt(2)
    centerx, centery = center
    rotation = 45*math.pi/180
    cosrot = math.cos(rotation)
    sinrot = math.sin(rotation)
    position = (centerx + height*cosrot/2-width*sinrot/2,
                centery-height*sinrot/2-width*cosrot/2)
    createRotatedRoundedRectangle(canvas, position, (width, height), 45,
                                  radius, color)
    drawLetter(canvas, (centerx, centery-2), 40, name, 'white')


def createDungeonItemLock(canvas, center, size, radius, color):
    width, height = size
    centerx, centery = center
    position = (centerx-width/2, centery-height/2)
    createRoundedRectangle(canvas, position, size, radius, color)
    drawItem(canvas, center, 'black')


def createDungeonItem(canvas, center, size, radius, color):
    width, height = size
    width = width/math.sqrt(2)
    height = height/math.sqrt(2)
    centerx, centery = center
    rotation = 45*math.pi/180
    cosrot = math.cos(rotation)
    sinrot = math.sin(rotation)
    position = (centerx + height*cosrot/2-width*sinrot/2,
                centery-height*sinrot/2-width*cosrot/2)
    createRotatedRoundedRectangle(canvas, position, (width, height), 45,
                                  radius, color)
    drawItem(canvas, center, 'white')


def createBossLock(canvas, center, size, radius, color):
    width, height = size
    centerx, centery = center
    position = (centerx-width/2, centery-height/2)
    createRoundedRectangle(canvas, position, size, radius, color)
    drawBoss(canvas, center, 'black', color)


def createBossKey(canvas, center, size, radius, color):
    width, height = size
    width = width/math.sqrt(2)
    height = height/math.sqrt(2)
    centerx, centery = center
    rotation = 45*math.pi/180
    cosrot = math.cos(rotation)
    sinrot = math.sin(rotation)
    position = (centerx + height*cosrot/2-width*sinrot/2,
                centery-height*sinrot/2-width*cosrot/2)
    createRotatedRoundedRectangle(canvas, position, (width, height), 45,
                                  radius, color)
    drawBoss(canvas, center, 'white', color)


def drawKey(canvas, center, color):
    centerx, centery = center
    radius = 7
    leftmost = 20
    toothheight = 5
    tooth2 = 14
    x = centerx - 11
    y = centery
    # loophole
    canvas.append(draw.ArcLine(x, -y, radius, 0, 360, stroke=color,
                  stroke_width=5, fill_opacity=0))
    canvas.append(draw.Line(x+radius, -y, x+radius+leftmost, -y,
                  stroke=color, stroke_width=5))
    canvas.append(draw.ArcLine(x+radius+leftmost, -y, 1, 0, 360,
                  stroke=color, stroke_width=3, fill=color, fill_opacity=1))
    # teeth
    canvas.append(draw.Line(x+radius+leftmost, -y, x+radius+leftmost,
                            -y-toothheight, stroke=color, stroke_width=5))
    canvas.append(draw.ArcLine(x+radius+leftmost, -y-toothheight, 1, 0, 360,
                               stroke=color, stroke_width=3, fill=color,
                               fill_opacity=1))
    canvas.append(draw.Line(x+radius+tooth2, -y, x+radius+tooth2,
                            -y-toothheight, stroke=color, stroke_width=5))
    canvas.append(draw.ArcLine(x+radius+tooth2, -y-5, 1, 0, 360, stroke=color,
                               stroke_width=3, fill=color, fill_opacity=1))


def drawItem(canvas, center, color):
    centerx, centery = center
    x = centerx - 10
    y = centery
    # x, y = position
    # bow
    canvas.append(draw.ArcLine(x, -y, 20, 290, 70, stroke=color,
                               stroke_width=3, fill='none'))
    # bowstring
    canvas.append(draw.Line(x + 10, -y + 20*math.sin(math.pi/3),
                            x + 10, -y - 20*math.sin(math.pi/3),
                            stroke=color, stroke_width=2))
    # arrow shaft
    canvas.append(draw.Line(x-8, -y, x+30, -y, stroke=color, stroke_width=1))
    # arrow tip
    canvas.append(draw.Lines(x + 30, -y + 3,
                             x + 35, -y,
                             x + 30, -y - 3,
                             fill=color))
    # feathers
    canvas.append(draw.Line(x - 10, -y + 3, x - 8, -y, stroke=color,
                            stroke_width=1))
    canvas.append(draw.Line(x - 10, -y - 3, x - 8, -y, stroke=color,
                            stroke_width=1))
    canvas.append(draw.Line(x-8, -y+3, x-6, -y, stroke=color,
                            stroke_width=1))
    canvas.append(draw.Line(x-8, -y-3, x-6, -y, stroke=color,
                            stroke_width=1))
    canvas.append(draw.Line(x-6, -y+3, x-4, -y, stroke=color,
                            stroke_width=1))
    canvas.append(draw.Line(x-6, -y-3, x-4, -y, stroke=color,
                            stroke_width=1))


def createNode(canvas, center, size, name, color):
    radius = size/2
    centerx, centery = center
    createCircle(canvas, center, radius, 0, 360, 'black', color)
    canvas.append(draw.Text(name, 13, centerx, -centery + 3, center=True,
                  fill='black'))


def drawBoss(canvas, center, color, background):
    centerx, centery = center
    radius = 7
    bottommost = 15
    toothheight = 5
    tooth2 = 9
    x = centerx
    y = centery - 5
    canvas.append(draw.ArcLine(x, -y, radius, 0, 360, stroke=color,
                               stroke_width=5, fill_opacity=0))
    canvas.append(draw.ArcLine(x, -y, 1, 0, 360, stroke=color,
                               stroke_width=5, fill_opacity=0))
    canvas.append(draw.Arc(x, -y+14, radius, 180, 360, stroke=color,
                           stroke_width=4, fill_opacity=0))
    canvas.append(draw.Arc(x, -y+17, radius, 197, 343, stroke=background,
                           stroke_width=5, fill=background,
                           fill_opacity=1))
    canvas.append(draw.Line(x, -y-radius, x, -y-radius-bottommost,
                            stroke=color, stroke_width=5))
    canvas.append(draw.ArcLine(x, -y-radius-bottommost, 1, 0, 360,
                               stroke=color, stroke_width=3,
                               fill=color, fill_opacity=1))

    canvas.append(draw.Line(x, -y-radius-bottommost,
                            x+toothheight, -y-radius-bottommost,
                            stroke=color, stroke_width=5))
    canvas.append(draw.ArcLine(x+toothheight, -y-radius-bottommost, 1, 0, 360,
                               stroke=color, stroke_width=3, fill=color,
                               fill_opacity=1))
    canvas.append(draw.Line(x, -y-radius-tooth2,
                            x+toothheight, -y-radius-tooth2, stroke=color,
                            stroke_width=5))
    canvas.append(draw.ArcLine(x+toothheight, -y-radius-tooth2, 1, 0, 360,
                               stroke=color, stroke_width=3, fill=color,
                               fill_opacity=1))


def findLargestRow(content, layers, lis):
    if isinstance(content, list):
        [findLargestRow(item, layers, lis) for item in content]
    elif isinstance(content, LOCK):
        lis.append(findLayer(content, layers))
        findLargestRow(content.getContent(), layers, lis)
    elif isinstance(content, KEY):
        lis.append(findLayer(content, layers))
    elif isinstance(content, str):
        lis.append(findLayer(content, layers))
    return lis


def mostFrequent(List):
    d = {}
    for item in List:
        if item in d:
            d[item] += 1
        else:
            d[item] = 1
    return max(d.items(), key=lambda x: x[1])


def separateByRow(content, layers):
    while content:
        layers.append([])
        if isinstance(content, list):
            layers[-1].extend([item for item in content
                               if (isinstance(item, KEY)
                                   or isinstance(item, str))])

        keys = [item for item in flatten(layers) if isinstance(item, KEY)]
        if isinstance(content, list):
            layers[-1].extend([item for item in content
                               if (isinstance(item, LOCK)
                                   and item.canUnlock(keys))])

        if isinstance(content, list):
            for item in layers[-1]:
                content.remove(item)
            [content.extend(item.getContent()) for item in layers[-1]
                if isinstance(item, LOCK)]
    layers = layers[1:]
    return layers


def separateByColumn(structure, columns):
    content = structure
    columnidx = 1
    while content != []:
        if isinstance(content, list) and len(content) > 0:
            item = content[0]
            while len(columns) <= columnidx:
                columns.append([])
            columns[columnidx].append(item)
            content = content[1:]
            if isinstance(item, LOCK):
                if len(content) == 0:
                    content = item.getContent()
                else:
                    content = item.getContent() + content
            if isinstance(item, KEY) or isinstance(item, str):
                columnidx += 1
        elif isinstance(content, LOCK):
            columns[columnidx].append(content)
        elif isinstance(content, KEY):
            columns[columnidx].append(content)
        elif isinstance(content, str):
            columns[columnidx].append(content)
    columns = columns[1:]
    return columns


def getHighestItem(List, rows):
    return min([(item, findLayer(item, rows)) for item in List],
               key=lambda tup: tup[1])[0]


def getLowestItem(List, rows):
    return max([(item, findLayer(item, rows)) for item in List],
               key=lambda tup: tup[1])[0]


def shiftRightColumns(columns, rows):
    columnidx = 1
    for colidx in range(2, len(columns)):
        currentcols = columns[colidx]
        currentidx = colidx
        previoushighestitem = (getHighestItem(columns[currentidx - 1], rows)
                               if columns[currentidx - 1] else None)
        while (previoushighestitem is None
               or findLayer(previoushighestitem, rows) >
               findLayer(getLowestItem(currentcols, rows), rows)):
            currentidx -= 1
            previoushighestitem = (getHighestItem(
                                                 columns[currentidx - 1], rows)
                                   if columns[currentidx - 1] else None)
        if colidx != currentidx:
            columns[currentidx].extend(currentcols)
            columns[colidx] = []


def graphNodes(canvas, parent, content, position, rows, columns):
    x, y = position
    verticalsep = 140
    horizontalsep = 90
    keypadding = 30
    iconsize = (60, 60)
    strokesize = 5
    if isinstance(content, list):
        for idx in range(len(content)):
            prevparentdepthx = findLayer(parent, columns)
            if idx > 0 and isinstance(content[idx-1], LOCK):
                prevdepthx = findLayer(content[idx-1], columns)
            else:
                prevdepthx = findLayer(content[0], columns)
            depthy = findLayer(content[idx], rows)
            parentdepthy = findLayer(parent, rows)
            depthx = findLayer(content[idx], columns)

            parentdepthx = findLayer(parent, columns)
            if isinstance(content[idx], LOCK):
                graphNodes(canvas, parent, content[idx],
                           (x+horizontalsep*(depthx-parentdepthx),
                            y+verticalsep*(depthy-parentdepthy)),
                           rows, columns)

            elif isinstance(content[idx], KEY):
                graphNodes(canvas, parent, content[idx],
                           (x+horizontalsep*(depthx-parentdepthx),
                            y+verticalsep-30),
                           rows, columns)

            elif isinstance(content[idx], str):
                graphNodes(canvas, parent, content[idx],
                           (x+horizontalsep*(depthx-parentdepthx),
                            y+verticalsep),
                           rows, columns)

            else:
                print('error')
    elif isinstance(content, LOCK):
        if isinstance(content, NORMLOCK):
            createNormalLock(canvas, position, iconsize, strokesize,
                             'red')

        elif isinstance(content, ITEMLOCK):
            createDungeonItemLock(canvas, position, iconsize, strokesize,
                                  'orange')

        elif isinstance(content, ITEMLOCK2):
            createDungeonItemLock(canvas, position, iconsize, strokesize,
                                  'blue')

        elif isinstance(content, MULTLOCK):
            createMultiLock(canvas, position, iconsize, strokesize,
                            content.getKeyhole(), 'purple')

        elif (isinstance(content, SPLOCK)
                and ord(content.getKeyhole()[0]) >= ALPHABETSTART):
            createSpecialLock(canvas, position, iconsize, strokesize,
                              content.getKeyhole(),
                              COLORS[ord(content.getKeyhole()[0])
                                     - ALPHABETSTART])

        elif isinstance(content, BOSSLOCK):
            createBossLock(canvas, position, iconsize, strokesize, '#1ac1ff')

        elif (isinstance(content, STLOCK)
              and ord(content.getKeyhole()[0]) >= NUMBERSTART):
            createStateLock(canvas, position, iconsize, strokesize,
                            content.getKeyhole(),
                            COLORS[ord(content.getKeyhole()[0]) - NUMBERSTART])

        elif isinstance(content, STARTLOCK):
            createNode(canvas, position, iconsize[0],
                       content.getKeyhole(), 'white')

        else:
            pass
        graphNodes(canvas, content, content.getContent(), position,
                   rows, columns)

    elif isinstance(content, KEY):
        if isinstance(content, NORMKEY):
            createNormalKey(canvas, position, iconsize, strokesize, 'red')

        elif isinstance(content, ITEMKEY):
            createDungeonItem(canvas, position, iconsize, strokesize, 'orange')

        elif isinstance(content, ITEMKEY2):
            createDungeonItem(canvas, position, iconsize, strokesize, 'blue')

        elif isinstance(content, MULTKEY):
            createMultiKey(canvas, position, iconsize, strokesize,
                           content.getName(), 'purple')

        elif (isinstance(content, SPKEY)
              and ord(content.getName()) >= ALPHABETSTART):
            createSpecialKey(canvas, position, iconsize, strokesize,
                             content.getName(),
                             COLORS[ord(content.getName()) - ALPHABETSTART])

        elif (isinstance(content, STKEY)
              and ord(content.getName()) >= NUMBERSTART):
            createStateKey(canvas, position, iconsize, strokesize,
                           content.getName(),
                           COLORS[ord(content.getName())-NUMBERSTART])

        elif isinstance(content, BOSSKEY):
            createBossKey(canvas, position, iconsize, strokesize, '#1ac1ff')

        elif isinstance(content, LOOPKEY):
            createMultiKey(canvas, position, iconsize, strokesize,
                           content.getName(), 'purple')

    elif isinstance(content, str):
        createNode(canvas, position, iconsize[0], content, 'white')
    else:
        print('Unexpected Content ' + str(content))


def graphLines(canvas, parent, content, position, rows, columns):
    x, y = position
    verticalsep = 140
    horizontalsep = 90
    keypadding = 30
    iconsize = (60, 60)
    strokesize = 5
    if isinstance(content, list):
        for idx in range(len(content)):
            depthy = findLayer(content[idx], rows)
            parentdepthy = findLayer(parent, rows)
            depthx = findLayer(content[idx], columns)
            parentdepthx = findLayer(parent, columns)
            if isinstance(content[idx], LOCK):

                if not isinstance(content[idx], STARTLOCK):
                    createLine(canvas, position, (x, y+verticalsep/2),
                               'white', 5)
                    createLine(canvas, (x, y+verticalsep/2),
                               (x+horizontalsep*(depthx-parentdepthx),
                                y+verticalsep/2), 'white', 5)
                    createCircle(canvas,
                                 (x+horizontalsep*(depthx-parentdepthx),
                                  y+verticalsep/2), 1, 0, 360,
                                 'white', 'white')
                    createLine(canvas,
                               (x+horizontalsep*(depthx-parentdepthx),
                                   y+verticalsep/2),
                               (x+horizontalsep*(depthx-parentdepthx),
                                   y+verticalsep*(depthy-parentdepthy)),
                               'white', 5)

                if content[idx].getLoopexit() is not None:
                    loopnode = [col[itemidx] for col in columns
                                for itemidx in range(len(col))
                                if (isinstance(col[itemidx], KEY) and
                                    col[itemidx].getName() ==
                                    content[idx].getLoopexit())]
                    loopdepthx = findLayer(loopnode[0], columns)
                    loopdepthy = findLayer(loopnode[0], rows)
                    createLine(canvas,
                               (x+horizontalsep*(depthx),
                                   y+verticalsep*(depthy)+keypadding/2),
                               (x+horizontalsep*(loopdepthx)+1.5*keypadding,
                                   y+verticalsep*(depthy)+keypadding/2),
                               'white', 5)
                    createCircle(canvas,
                                 (x+horizontalsep*(loopdepthx)+1.5*keypadding,
                                     y+verticalsep*(depthy)+keypadding/2),
                                 1, 0, 360, 'white', 'white')
                    createLine(canvas,
                               (x+horizontalsep*(loopdepthx)+1.5*keypadding,
                                   y+verticalsep*(depthy)+keypadding/2),
                               (x+horizontalsep*(loopdepthx)+1.5*keypadding,
                                   y+verticalsep*(loopdepthy)-keypadding),
                               'white', 5)
                    createCircle(canvas,
                                 (x+horizontalsep*(loopdepthx)+1.5*keypadding,
                                     y+verticalsep*(loopdepthy)-keypadding),
                                 1, 0, 360, 'white', 'white')
                    createLine(canvas,
                               (x+horizontalsep*(loopdepthx)+1.5*keypadding,
                                   y+verticalsep*(loopdepthy)-keypadding),
                               (x+horizontalsep*(loopdepthx),
                                   y+verticalsep*(loopdepthy)-keypadding),
                               'white', 5)
                graphLines(canvas, content[idx], content[idx],
                           (x+horizontalsep*(depthx-parentdepthx),
                               y+verticalsep*(depthy-parentdepthy)),
                           rows, columns)

            elif isinstance(content[idx], KEY):
                createLine(canvas, position, (x, y+verticalsep/2), 'white', 5)
                createLine(canvas,
                           (x, y+verticalsep/2),
                           (x+horizontalsep*(depthx-parentdepthx),
                               y+verticalsep/2),
                           'white', 5)
                createCircle(canvas,
                             (x+horizontalsep*(depthx-parentdepthx),
                                 y+verticalsep/2),
                             1, 0, 360, 'white', 'white')
                createLine(canvas,
                           (x+horizontalsep*(depthx-parentdepthx),
                               y+verticalsep/2),
                           (x+horizontalsep*(depthx-parentdepthx),
                               y+verticalsep-30),
                           'white', 5)
                graphLines(canvas, content[idx], content[idx],
                           (x+horizontalsep*(depthx-parentdepthx),
                               y+verticalsep-30),
                           rows, columns)

            elif isinstance(content[idx], str):
                createLine(canvas, position,
                           (x+horizontalsep*(depthx-parentdepthx),
                               y+verticalsep),
                           'white', 5)
                graphLines(canvas, content[idx], content[idx],
                           (x+horizontalsep*(depthx-parentdepthx),
                               y+verticalsep),
                           rows, columns)

            else:
                print('error')
    elif isinstance(content, LOCK):
        graphLines(canvas, content, content.getContent(), position,
                   rows, columns)
    elif isinstance(content, KEY):
        pass
    elif isinstance(content, str):
        pass
    else:
        print('Unexpected Content: ' + str(content))


def lockKeyCompare(rows, obj):
    size = len(rows)
    maxdepth = getDepth(rows[1])
    if isinstance(obj, list):
        return (len(obj), obj)
    elif isinstance(obj, LOCK):
        return (size - findLayer(obj, rows), maxdepth - getDepth(obj))
    elif isinstance(obj, KEY):
        return (size - findLayer(obj, rows), maxdepth - getDepth(obj))
    elif isinstance(obj, str):
        return (0, obj)


def recSort(content, rows):
    partialLockKeyCompare = partial(lockKeyCompare, rows)
    if isinstance(content, list):
        [recSort(item, rows) for item in content if isinstance(item, LOCK)]
        content.sort(key=partialLockKeyCompare)
    elif isinstance(content, LOCK):
        recSort(content.getContent(), rows)
    else:
        pass


def findLayer(item, layers):
    if layers is not None:
        for i in range(len(layers)):
            if layers[i] is not None and item in layers[i]:
                return i
    return 0


def graphDungeon(dungeon, outputname, outputtype):
    # We copy the dungeon to prevent messing with the original
    content = copy.deepcopy(dungeon.getContent())
    # separate the dungeon into rows
    rows = separateByRow(content, [dungeon.getKeys()])
    # use that separation to sort the dungeon to keep the biggest
    # parts to the left
    newcontent = rows[0]
    recSort(newcontent, rows)
    # separate the dungeon into columns
    columns = separateByColumn(newcontent, [])
    # Move to the left the contents if they do not collide
    # with anything
    shiftRightColumns(columns, rows)
    columns = [col for col in columns if col != []]
    # prepare the sizes for the creation of the canvas using
    # the information from rows and columns
    height = len(rows)*140
    width = (len(columns)+0.5)*90
    canvas = createCanvas((width, height))
    # graph the lines first so that they are on the bottom and then the nodes
    createRectangle(canvas, (0, 0), (width, height), '#808080')
    graphLines(canvas, rows[0][0], [columns[0][0]], (60, 60), rows, columns)
    graphNodes(canvas, rows[0][0], [columns[0][0]], (60, 60), rows, columns)
    # set the resolution of the image
    canvas.setPixelScale(4)
    # save
    if outputtype == 'both':
        canvas.savePng(outputname+'.png')
        canvas.saveSvg(outputname+'.svg')
    elif outputtype == 'png':
        canvas.savePng(outputname+'.png')
    elif outputtype == 'svg':
        canvas.saveSvg(outputname+'.svg')
    else:
        raise Exception('Unsupported output type: ' + str(outputtype))
