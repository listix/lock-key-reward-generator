#!/usr/bin/python3

import copy
import itertools

LOCKS = 'locks'
KEYS = 'keys'
REWARDS = 'rewards'
LOOP = 'loop'
ACTIONS = 'actions'

BOSS = 'BOSS'
START = 'Start'
NORMAL = 'K'

MIXED = 'mixed'
ONLYKEYS = 'onlykeys'
ONLYLOCKS = 'onlyMIXEDlocks'

KEYICON = '⚷'
LOCKICON = '!'

UNLIMITED = -1
ONETIME = 1
PARTIALKEYS = False

# Node class


class KEY(object):
    '''Key class. It can have many uses(1 by default) or unlimited ones.'''
    def __init__(self, name, uses=1):
        if isinstance(name, list) and len(name) == 1:
            self.name = name[0]
        else:
            self.name = name
        self.uses = uses

    def __str__(self):
        string = None
        if self.uses > 0:
            string = KEYICON + self.name
        elif self.uses == UNLIMITED:
            string = '(' + KEYICON + self.name + ')'
        else:
            string = '(' + KEYICON + self.name + ',' + str(self.uses) + ')'
        return string

    def __repr__(self):
        return self.__str__()

    def getName(self):
        return self.name

    def getUsesLeft(self):
        return self.uses

    def useKey(self):
        if self.uses > 0:
            self.uses -= 1
        elif self.uses == 0:
            raise Exception('Error, key '
                            + str(self.name) + ' is already spent')


class NORMKEY(KEY):
    def __init__(self):
        super().__init__(NORMAL, 1)


class STKEY(KEY):
    def __init__(self, name):
        super().__init__(name, UNLIMITED)


class SPKEY(KEY):
    def __init__(self, name):
        super().__init__(name, 1)


class ITEMKEY(KEY):
    def __init__(self, name):
        super().__init__(name, UNLIMITED)


class ITEMKEY2(KEY):
    def __init__(self, name):
        super().__init__(name, UNLIMITED)


class MULTKEY(KEY):
    def __init__(self, name, uses=1):
        super().__init__(name, uses)


class LOOPKEY(KEY):
    def __init__(self, name):
        super().__init__(name, 1)


class BOSSKEY(KEY):
    def __init__(self):
        super().__init__(BOSS, 1)


class STARTKEY(KEY):
    def __init__(self):
        super().__init__(START, 1)


class LOCK(object):
    '''Lock class. It can have multiple uses(1 by default) or unlimited(in
    this case this can be thought of as a Status lock). Unlimited locks
    propagate its keyhole to the underlying locks to maintain its requirements
    and prevent erroneous solvings.'''
    def __init__(self, keyhole, contents, uses=1, loopexit=None):
        if isinstance(keyhole, str):
            self.keyhole = [keyhole]
        if isinstance(keyhole, list):
            self.keyhole = keyhole
        self.contents = []
        if isinstance(contents, list):
            self.contents.extend(contents)
        else:
            self.contents.append(contents)

        self.uses = uses
        self.loopexit = loopexit

    def getKeyhole(self):
        return self.keyhole

    def __str__(self):
        result = LOCKICON
        if len(self.keyhole) == 1:
            result += str(self.keyhole[0]) + '->'
        else:
            result += str(self.keyhole) + '->'
        if isinstance(self.contents, list):
            if len(self.contents) == 1:
                result += str(self.contents[0])
            else:
                result += str(self.contents)

#        if isinstance(self.contents,LOCK) or isinstance(self.contents,KEY):
#            result += self.contents.__str__()
#        else:
#            result += str(self.contents)
        return result + ''

    def __repr__(self):
        return self.__str__()

    def unlock(self, item):
        '''Unlocks a lock. This function operates under the assumption that
        you can actually unlock the lock'''
        if isinstance(item, list):
            for it in item:
                it.useKey()
                (self.keyhole).remove(it.name)
            if len(self.keyhole) == 0:
                return self.contents
            else:
                raise Exception('The key(s) provided didnt open the lock.'
                                + 'Lock: ' + str(self.keyhole) + ' Keys:'
                                + str(item))
        else:
            print('Trying to open a lock with a non key object' + str(item))
            return -1

    def canUnlock(self, keys):
        keynames = [k.name for k in keys]
        keyholes = copy.copy(self.keyhole)
        for k in keynames:
            if k in keyholes:
                keyholes.remove(k)
        if keyholes == []:
            return True
        else:
            return False

    def getUsesLeft(self):
        return self.uses

    def getContent(self):
        return self.contents

    def getContentLocks(self):
        return [lock for lock in self.getContent() if isinstance(lock, LOCK)]

    def getContentKeys(self):
        return [key for key in self.getContent() if isinstance(key, KEY)]

    def getLoopexit(self):
        return self.loopexit

    def setLoopexit(self, value):
        self.loopexit = value

    def findRequiredKeys(self, currentkeys):
        '''Intersection of the currentkeys we have and
        the keys the lock needs.'''
        keys = []
        keyholes = copy.copy(self.getKeyhole())
        for k in currentkeys:
            if k.name in keyholes:
                keyholes.remove(k.name)
                keys.append(k)
        return keys


class DUNGEON(object):
    '''Dungeon class. This class'''
    def __init__(self, startingkey, startinglock):
        if isinstance(startingkey, KEY):
            self.keys = [startingkey]
        elif isinstance(startingkey, list):
            self.keys = startingkey
        if isinstance(startinglock, LOCK):
            self.content = [startinglock]
        elif isinstance(startinglock, list):
            self.content = startinglock
        self.actions = []
        self.rewards = []
        self.loop = False

    def __str__(self):
        string = '' if self.getKeys() == [] else 'Current keys: ' \
                + str(self.getKeys()) + '\n'
        string += 'Current contents: ' + str(self.getContent())
        return string

    def __repr__(self):
        return self.__str__()

    def getContent(self):
        return self.content

    def getContentLocks(self):
        return [lock for lock in self.getContent() if isinstance(lock, LOCK)]

    def getContentKeys(self):
        return [key for key in self.getContent() if isinstance(key, KEY)]

    def getKeys(self):
        return self.keys

    def getRewards(self):
        return self.rewards

    def addKey(self, key):
        self.keys.append(key)

    def removeKey(self, key):
        self.keys.remove(key)

    def removeContentValue(self, key):
        self.content.remove(key)

    def addAction(self, action):
        self.actions.append(action)

    def getActions(self):
        return self.actions

    def addReward(self, reward):
        self.rewards.append(reward)

    def setContent(self, content):
        if isinstance(content, LOCK):
            self.content = [content]
        elif isinstance(content, list):
            self.content = content

    def addContent(self, content):
        if isinstance(content, list):
            self.content.extend(content)
            self.content.sort(key=lockKeyComparator, reverse=True)
        else:
            self.content.append(content)
            self.content.sort(key=lockKeyComparator, reverse=True)

    def removeLock(self, lock):
        self.content.remove(lock)

    def setLoop(self, loop):
        self.loop = loop

    def getLoop(self):
        return self.loop

    def step(self):
        '''This solves a single step of a dungeon.
        It will take the first option it finds.'''
        keys = None
        for lock in self.getContentLocks():
            keys = lock.findRequiredKeys(self.getKeys())
            if lock.canUnlock(keys):
                self.unlockProcess(lock, keys)
                break
        if len(keys) == 0:
            print('Nothing else can be done with this statement')

    def unlockProcess(self, lock, keys):
        '''This are the steps required to unlock a lock, remove the lock,
        remove the used keys and then call the function to sort out the
        contents. It also creates a new lock if there is a one way lock'''
        keyhole = copy.copy(lock.getKeyhole())
        # when calling this function we already know that we can unlock the
        # lock. Hence why we are not checking again.
        contents = lock.unlock(keys)
        self.addAction('Use key' + ((' ' + keys[0].name)
                                    if len(keys) == 1
                                    else ('s '
                                          + str([k.name for k in keys]))))
        self.removeLock(lock)
        for key in keys:
            if key.getUsesLeft() == 0:
                # print('Removing key ' + str(key))
                self.removeKey(key)
            # When we want to propagate the infinte locks we remove the locks
            # that arent infinite
            if key.getUsesLeft() != UNLIMITED:
                keyhole.remove(key.name)
        # When there is a loopexit key we put all the other locks under a
        # keyhole with that loopexitname
        loopexit = lock.loopexit
        if loopexit is not None:
            self.setContent([LOCK(lock.loopexit, self.getContent())])
        lockuses = lock.getUsesLeft()
        self.separateContents(contents, lockuses, keyhole)

    def separateContents(self, contents, locktype, keyhole):
        '''Once a lock is opened the contents need to be separated depending
        on their type. State locks(known as unlimited locks) propagate their
        keyhole to their content locks'''
        if isinstance(contents, LOCK):
            if locktype == UNLIMITED:
                newlock = LOCK(keyhole
                               + contents.keyhole, contents.contents,
                               UNLIMITED)
                self.addContent(newlock)
            else:
                self.addContent(contents)
        elif isinstance(contents, KEY):
            if PARTIALKEYS is True:
                self.addContent(contents)
            else:
                self.addAction('Get key ' + str(contents.name))
                self.addKey(contents)
        elif isinstance(contents, str):
            self.addAction('Get reward ' + str(contents))
            self.addReward(contents)
        elif isinstance(contents, list):
            for c in contents:
                self.separateContents(c, locktype, keyhole)
        else:
            raise Exception('Content that is neither KEY,LOCK or STR')
        return self

    def isSolvable(self):
        '''This function solves a dungeon in the first way it finds'''
        [self.addKey(k) for k in self.getContentKeys()]
        stp = 1
        while any(lock.findRequiredKeys(self.getKeys()) != []
                  for lock in self.getContentLocks()):
            beforestep = str(self.getContent())
            beforeactions = len(self.getActions())
            self.step()
            afterstep = str(self.getContent())
            stp += 1
            if beforestep == afterstep:
                # TODO
                print('Loop detected, exiting solver')
                return False
        for k in self.getKeys():
            if k.uses != UNLIMITED:
                print('Warning! Key: ' + str(k) + ' is never used.')
        for locks in self.getContentLocks():
            print('Warning! Lock: ' + str(locks) + ' is never opened.')
        if self.getRewards() == []:
            return False
        return True

    def solve(self):
        '''This function solves a dungeon in the first way it finds'''
        [self.addKey(k) for k in self.getContentKeys()]
        stp = 1
        while any(lock.findRequiredKeys(self.getKeys()) != []
                  for lock in self.getContentLocks()):
            print(self)
            beforestep = str(self.getContent())
            beforeactions = len(self.getActions())
            self.step()
            afterstep = str(self.getContent())
            print('Action ' + str(stp) + ': '
                  + str(self.getActions()[beforeactions:]))
            print()
            stp += 1
            if beforestep == afterstep:
                print('Loop detected, exiting solver')
                break
        for key in self.getKeys():
            if key.uses != UNLIMITED:
                print('Warning! Key: ' + str(key) + ' is never used.')
        for locks in self.getContentLocks():
            print('Warning! Lock: ' + str(locks) + ' is never opened.')
        if self.getRewards() == []:
            print('No rewards obtained')

    def unlockableLocksExist(self):
        '''Find if there are still unlockable locks with the current keys'''
        currentkeys = self.getKeys()
        availablekeys = self.getContentKeys()
        availablelocks = self.getContentLocks()
        for lock in availablelocks:
            if lock.canUnlock(currentkeys+availablekeys):
                return True
        return False

    def traverseAvailablePaths(self):
        '''As long as there are unlockable locks and not a loop this function
        will solve the dungeon in all ways possible.'''
        paths = [[self]]
        while any([p[-1].getLoop() != [] and p[-1].unlockableLocksExist()
                  for p in paths]):
            for dungeon in [p for p in paths
                            if p[-1].getLoop() != []
                            and p[-1].unlockableLocksExist()]:
                newpaths = self.solveAvailablePaths(dungeon)
                for np in newpaths:
                    if str(np[-1]) == str(np[-2]):
                        np[-1].setLoop(True)
                        print('Loop Detected')
                paths.remove(dungeon)
                paths += newpaths
        return paths

    def isCompletelySolvable(self):
        availablepaths = self.traverseAvailablePaths()
        for pathidx in range(len(availablepaths)):
            nonusedkeys = [key for key in availablepaths[pathidx][-1].getKeys()
                           if key.getUsesLeft() != UNLIMITED]
            nonopenedlocks = [lock
                              for lock
                              in availablepaths[pathidx][-1].getContentLocks()]
            if len(nonopenedlocks) > 0:
                return False
        return True

    def solveDungeonAll(self):
        '''This function solves and prints all the paths that can be taken to
        solve the dungeon.
            Warning this function may take a long time to complete!'''
        availablepaths = self.traverseAvailablePaths()
        for pathidx in range(len(availablepaths)):
            print('---- Path ' + str(pathidx + 1) + ' ----')
            for stepidx in range(len(availablepaths[pathidx])):
                beforeactions = len(availablepaths[pathidx][stepidx - 1]
                                    .getActions())
                print('Action '
                      + str(stepidx + 1) + ': '
                      + str(availablepaths[pathidx][stepidx]
                            .getActions()[beforeactions:]))
                print('Current keys: '
                      + str(availablepaths[pathidx][stepidx].getKeys()))
                print('Dungeon Status: '
                      + str(availablepaths[pathidx][stepidx].getContent()))
                print()
            if availablepaths[pathidx][-1].getLoop():
                print('Loop detected')
            nonusedkeys = [key for key in availablepaths[pathidx][-1].getKeys()
                           if key.getUsesLeft() != UNLIMITED]
            if len(nonusedkeys) == 1:
                print('Warning! Key: '
                      + str(nonusedkeys) + ' is never used.')
            elif len(nonusedkeys) > 1:
                print('Warning! Keys: ' + str(nonusedkeys)
                      + ' are never used.')
            nonopenedlocks = [lock
                              for lock
                              in availablepaths[pathidx][-1].getContentLocks()
                              ]
            if len(nonopenedlocks) == 1:
                print('Warning! Lock: ' + str(nonopenedlocks)
                      + ' is never opened.')
            elif len(nonopenedlocks) > 1:
                print('Warning! Locks: ' + str(nonopenedlocks)
                      + ' are never opened.')
        endingstate = [state[-1] for state in availablepaths
                       if state[-1].getRewards() == []]
        print('There are ' + str(len(endingstate))
              + ' paths that ended without rewards')
        print('Key/Lock count: ' + str(getNodesCount(self.getContent())))
        print('Depth: ' + str(getDepth(self.getContent())))
        print('Complexity: ' + str(sum([len(p) for p in availablepaths])))
        print('--------------------------------------------------------------')

    def solveAvailablePaths(self, currentpath):
        '''For a current dungeon status it tries to solve one step in every way
        possible. If PARTIALKEYS is True it will also get 1 or more
        combinations of the keys instead of all of them at once. This change
        makes the overall execution of the program much slower'''
        keys = None
        paths = []
        statement = currentpath[-1]
        if PARTIALKEYS is True:
            keyindexes = []
            currentstatement = None
            lcontent = statement.getContentKeys()
            for itemidx in range(len(lcontent)):
                keyindexes.append(itemidx)
            if len(keyindexes) != 0:
                for powersetkey in powerset(keyindexes):
                    if powersetkey != []:
                        currentstatement = copy.deepcopy(statement)
                        availablekeys = [currentstatement
                                         .getContentKeys()[idx]
                                         for idx in powersetkey]
                        for k in availablekeys:
                            statement.addAction('Get key '+str(k.name))
                            currentstatement.addKey(k)
                            currentstatement.removeContentValue(k)
                        newpath = copy.copy(currentpath)
                        newpath.append(currentstatement)
                        paths.append(newpath)

        for lockidx in range(len(statement.getContentLocks())):
            lock = statement.getContentLocks()[lockidx]
            if lock.canUnlock(statement.getKeys()):
                currentstatement = copy.deepcopy(statement)
                currentlock = currentstatement.getContentLocks()[lockidx]
                currentkeys = currentlock.findRequiredKeys(
                                currentstatement.getKeys())
                currentstatement.unlockProcess(currentlock, currentkeys)
                newpath = copy.copy(currentpath)
                newpath.append(currentstatement)
                paths.append(newpath)

        if len(paths) == 0:
            paths = [currentpath + [currentpath[-1]]]

        return paths


class NORMLOCK(LOCK):
    def __init__(self, contents,loopexit=None):
        super().__init__(NORMAL, contents, 1,loopexit)


class SPLOCK(LOCK):
    def __init__(self, keyhole, contents, loopexit=None):
        super().__init__(keyhole, contents, 1, loopexit)


class STLOCK(LOCK):
    def __init__(self, keyhole, contents, loopexit=None):
        super().__init__(keyhole, contents, UNLIMITED, loopexit)


class ITEMLOCK(LOCK):
    def __init__(self, keyhole, contents, loopexit=None):
        super().__init__(keyhole, contents, 1, loopexit)


class ITEMLOCK2(LOCK):
    def __init__(self, keyhole, contents, loopexit=None):
        super().__init__(keyhole, contents, 1, loopexit)


class MULTLOCK(LOCK):
    def __init__(self, keyhole, contents, loopexit=None):
        super().__init__(keyhole, contents, 1, loopexit)


class LOOPLOCK(LOCK):
    def __init__(self, keyhole, contents, loopexit=None):
        super().__init__(keyhole, contents, 1, loopexit)


class BOSSLOCK(LOCK):
    def __init__(self, contents):
        super().__init__(BOSS, contents, 1)


class STARTLOCK(LOCK):
    def __init__(self, contents):
        super().__init__(START, contents, 1)


def getDepth(structure, depth=0):
    '''This finds the deepest sequence of Locks in the structure'''
    if isinstance(structure, list):
        result = [getDepth(item, depth) for item in structure] + [0]
        return max(result)
    elif isinstance(structure, LOCK):
        return getDepth(structure.contents, depth + 1)
    elif isinstance(structure, KEY) or isinstance(structure, str):
        return depth
    else:
        print('The structure was unexpected ' + str(structure))


def getNodesCount(structure):
    '''Check how many keys and locks there are in the structure'''
    if isinstance(structure, list):
        return sum([(1 + getNodesCount(item.contents))
                    if isinstance(item, LOCK)
                    else (1 if isinstance(item, KEY) else 0)
                    for item in structure])
    elif isinstance(structure, LOCK):
        return 1 + getNodesCount(structure.contents)
    elif isinstance(structure, KEY):
        return 1
    elif isinstance(structure, str):
        return 0


def powerset(iterable):
    '''Returns the powerset of a list'''
    s = list(iterable)
    result = list(dict.fromkeys(itertools.chain
                                .from_iterable(itertools
                                               .combinations(s, r)
                                               for r in range(len(s)+1))))
    return [list(v) for v in result]


def lockKeyComparator(obj):
    if isinstance(obj, list):
        return (3, len(obj))
    elif isinstance(obj, LOCK):
        if len(findReward('Exit', obj)) != 0:
            return (4, getDepth(obj))
        else:
            return (2, getDepth(obj), obj.getKeyhole())
    elif isinstance(obj, KEY):
        return (1, obj.getUsesLeft(), obj.getName())
    elif isinstance(obj, str):
        return (0, obj)
    else:
        raise Exception('Error, object '
                        + str(obj)
                        + ' wasn''t expected.')


def flatten(lis):
    flatlist = []
    for elem in lis:
        if isinstance(elem, list):
            flatlist.extend(flatten(elem))
        else:
            flatlist.append(elem)
    return flatlist


def findReward(reward, content):
    if isinstance(content, list):
        result = [findReward(reward, item) for item in content]
        result = list(filter(None.__ne__, flatten(result)))
        return result
    elif isinstance(content, LOCK):
        return findReward(reward, content.getContent())
    elif isinstance(content, KEY):
        return None
    elif isinstance(content, str):
        if content == reward:
            return content
        else:
            return None
    else:
        print('findUnlimitedKeys The structure was pected ' + str(content))


def recursiveSort(content, reverse):
    print(content)
    if isinstance(content, list):
        [recursiveSort(item, reverse) for item in content]
        content.sort(key=lockKeyComparator, reverse=reverse)
    elif isinstance(content, LOCK):
        recursiveSort(content.getContent(), reverse)
    else:
        pass
