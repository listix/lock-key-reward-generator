#!/usr/bin/python3

import random
import copy
from solver import KEY
from solver import NORMKEY
from solver import STKEY
from solver import SPKEY
from solver import ITEMKEY
from solver import ITEMKEY2
from solver import MULTKEY
from solver import LOOPKEY
from solver import BOSSKEY
from solver import STARTKEY

from solver import LOCK
from solver import NORMLOCK
from solver import STLOCK
from solver import SPLOCK
from solver import ITEMLOCK
from solver import ITEMLOCK2
from solver import MULTLOCK
from solver import BOSSLOCK
from solver import STARTLOCK

from solver import DUNGEON
from solver import UNLIMITED
from solver import ONETIME
from solver import flatten

SMALLKEY = 'k'
SPECIALKEY = 'SP'
DKEY = 'DKEY'
D2KEY = 'D2KEY'
LKEY = 'c'
MULTIKEY = 'M'
STATEKEY = 'STATE'

MINMULTIKEY = 2
MAXMULTIKEY = 3

OTHER = 'other'
MIXED = 'mixed'
ONLYKEYS = 'onlykeys'
ONLYLOCKS = 'onlylocks'


def genSPKeyName(number):
    return chr(64+number)


def genStateKeyName(number):
    return chr(47+number)


def genDungeonItem(dungeon):
    pass


def getMultiKey():
    return [MULTIKEY]*random.randint(MINMULTIKEY, MAXMULTIKEY)


def generateLockOptions(normlockmax, spkeymax, dkeymax, d2keymax, multikeymax,
                        statekeymax, contents):
    keyoptions = [SMALLKEY] if normlockmax > 0 \
                 and not isRedundantLock(SMALLKEY, contents) else [] \
                 + ([STATEKEY] if statekeymax > 0 else []) \
                 + ([SPECIALKEY] if spkeymax > 0 else []) \
                 + ([DKEY] if dkeymax > 0 else []) \
                 + ([D2KEY] if d2keymax > 0 else []) \
                 + ([getMultiKey()] if multikeymax > 0 else [])
    return keyoptions


def isRedundantLock(keyname, contents):
    deepestContent = findDeepestKey(contents)
    if isinstance(deepestContent, KEY) and keyname == deepestContent.getName():
        return True
    else:
        return False


def findDeepestContent(content, depth):
    if isinstance(content, list):
        return [findDeepestContent(item, depth+1)[0] for item in content]
    elif isinstance(content, LOCK):
        return findDeepestContent(content.getContent(), depth+1)
    elif isinstance(content, KEY):
        return (content, depth)
    elif isinstance(content, str):
        return (content, 0)
    else:
        print('findDeepestContent: The structure was unexpected '
              + str(content))


def findDeepestKey(content):
    result = findDeepestContent(content, 0)
    if isinstance(result, list):
        if len(result) == 1:
            return result[0]
    return result


def findUnlimitedKeys(keyname, content):
    if isinstance(content, list):
        result = [findUnlimitedKeys(keyname, item) for item in content]
        result = list(filter(None.__ne__, flatten(result)))
        return result
    elif isinstance(content, LOCK):
        return findUnlimitedKeys(keyname, content.getContent())
    elif isinstance(content, KEY):
        if content.getUsesLeft() == UNLIMITED and content.getName() == keyname:
            return content
        else:
            return None
    elif isinstance(content, str):
        return None
    else:
        print('The structure was unexpected ' + str(content))


def findUnlimitedLocks(content):
    if isinstance(content, list):
        result = [findUnlimitedLocks(item) for item in content
                  if isinstance(item, LOCK)]
        result = list(filter(None.__ne__, flatten(result)))
        return result
    elif isinstance(content, LOCK):
        if content.getUsesLeft() == UNLIMITED:
            return content
        else:
            return findUnlimitedLocks(content.getContent())
    elif isinstance(content, KEY):
        return None
    elif isinstance(content, str):
        return None
    else:
        print('The structure was unexpected ' + str(content))


def validateAndAddOneWayLock(dungeon, selecteditems, key, lock, looplock):
    content = None
    loopresult = None
    if looplock:
        startingkeys = [KEY('')] + ([] if isKeyPresent(DKEY, selecteditems)
                                    else [ITEMKEY(DKEY)])
        startingkeys += ([] if isKeyPresent(D2KEY, selecteditems)
                         else [ITEMKEY2(D2KEY)])
        minidungeon = DUNGEON(startingkeys,
                              LOCK('', copy.deepcopy(selecteditems)))
        deepestlock = findDeepestLock(selecteditems, 0)
        statelock = findUnlimitedLocks(selecteditems)
        if (deepestlock
           and len(statelock) == 0
           and minidungeon.isCompletelySolvable()
           and len(dungeon.getContent()) > 0
           and minidungeon.getRewards() == []):
            # We add a LOOPKEY at the bottom of the structure so that is the
            # point where we are supposed to go back.
            addDeepestKey(LKEY, selecteditems)
            print('Added loop key at the bottom' + str(selecteditems))
            lock[0].setLoopexit(LKEY)
            content = dungeon.getContent() + key + lock
            loopresult = looplock
            addresult = 1
        else:
            print('Adding a loop lock failed.' +
                  'Returning dungeon to its previous state.')
            content = dungeon.getContent() + selecteditems
            loopresult = 0
            addresult = 0
    else:
        # We need to check if there are no statelocks in the selected items
        statelock = findUnlimitedLocks(selecteditems)
        if len(statelock) == 0:
            content = dungeon.getContent() + key + lock
            loopresult = looplock
            addresult = 1
        else:
            content = dungeon.getContent() + selecteditems
            loopresult = 0
            addresult = 0

    return (content, loopresult, addresult)


def verifyAndApplyChanges(dungeon, content, selecteditems, usedloop, addedkey):
    # Just a fictional start state with a blank key and (if it doesnt exist)
    # the dungeon key.
    fictionalkeys = [KEY('')] + ([] if isKeyPresent(DKEY, content)
                                 else [ITEMKEY(DKEY)])
    fictionalkeys += ([] if isKeyPresent(D2KEY, content)
                      else [ITEMKEY2(D2KEY)])
    # We make an overkill sanity check when adding a normal key. The dungeon
    # must be solvable from this point
    fictionaldungeon = DUNGEON(fictionalkeys, LOCK('', copy.deepcopy(content)))
    if fictionaldungeon.isCompletelySolvable():
        dungeon.setContent(content)
        result = (usedloop, addedkey)
    else:
        print('There will be an unsolvable state' +
              'if we lock this inside the normal lock')
        print('Items tried to be put under a lock: ' + str(selecteditems))
        print('We are reverting the changes and' +
              'continuing as if nothing happened.')
        # If there was a LOOPKEY that was added we remove it.
        removeKey(LKEY, selecteditems)
        dungeon.addContent(selecteditems)
        result = (0, 0)

    return result


def addMultiLock(dungeon, keyname, selecteditems, looplock):
    for item in selecteditems:
        dungeon.removeContentValue(item)

    (content,
     usedloop,
     addedkey) = validateAndAddOneWayLock(dungeon,
                                          selecteditems,
                                          [MULTKEY(key) for key in keyname],
                                          [MULTLOCK(keyname, selecteditems)],
                                          looplock)
    result = verifyAndApplyChanges(dungeon,
                                   content,
                                   selecteditems,
                                   usedloop,
                                   addedkey)

    return result


def addSpecialLock(dungeon, keyname, selecteditems, looplock):
    for item in selecteditems:
        dungeon.removeContentValue(item)

    (content,
     usedloop,
     addedkey) = validateAndAddOneWayLock(dungeon,
                                          selecteditems,
                                          [SPKEY(keyname)],
                                          [SPLOCK(keyname, selecteditems)],
                                          looplock)
    result = verifyAndApplyChanges(dungeon,
                                   content,
                                   selecteditems,
                                   usedloop,
                                   addedkey)

    return result


def addStateLock(dungeon, keyname, selecteditems, looplock):
    for item in selecteditems:
        dungeon.removeContentValue(item)

    (content,
     usedloop,
     addedkey) = validateAndAddOneWayLock(dungeon,
                                          selecteditems,
                                          [STKEY(keyname)],
                                          [STLOCK(keyname, selecteditems)],
                                          looplock)
    result = verifyAndApplyChanges(dungeon,
                                   content,
                                   selecteditems,
                                   usedloop,
                                   addedkey)

    return result


def addDungeonItemLock(dungeon, keyname, selecteditems, includeDungeonKey,
                       looplock):
    result = (0, 0)
    if includeDungeonKey:
        dungeon.addContent(ITEMKEY(keyname))
        result = (0, 1)
    else:
        for item in selecteditems:
            dungeon.removeContentValue(item)

        (content,
         usedloop,
         addedkey) = validateAndAddOneWayLock(dungeon,
                                              selecteditems,
                                              [],
                                              [ITEMLOCK(keyname,
                                                        selecteditems)],
                                              looplock)
        result = verifyAndApplyChanges(dungeon,
                                       content,
                                       selecteditems,
                                       usedloop,
                                       addedkey)

    return result


def addDungeonItemLock2(dungeon, keyname, selecteditems, includeDungeonKey,
                        looplock):
    result = (0, 0)
    if includeDungeonKey:
        dungeon.addContent(ITEMKEY2(keyname))
        result = (0, 1)
    else:
        for item in selecteditems:
            dungeon.removeContentValue(item)

        (content,
         usedloop,
         addedkey) = validateAndAddOneWayLock(dungeon,
                                              selecteditems,
                                              [],
                                              [ITEMLOCK2(keyname,
                                                         selecteditems)],
                                              looplock)
        result = verifyAndApplyChanges(dungeon,
                                       content,
                                       selecteditems,
                                       usedloop,
                                       addedkey)

    return result


def addNormalLock(dungeon, selecteditems, looplock):
    for item in selecteditems:
        dungeon.removeContentValue(item)

    (content,
     usedloop,
     addedkey) = validateAndAddOneWayLock(dungeon,
                                          selecteditems,
                                          [NORMKEY()],
                                          [NORMLOCK(selecteditems)],
                                          looplock)
    result = verifyAndApplyChanges(dungeon,
                                   content,
                                   selecteditems,
                                   usedloop,
                                   addedkey)

    return result


def addDeepestKey(keyname, contents):
    deepestlock = findDeepestLock(contents, 0)
    if deepestlock is not None:
        lock = deepestlock[0]
        if isinstance(lock.contents, list):
            lock.contents.append(LOOPKEY(keyname))
        else:
            lock.contents = [lock.contents] + [KEY(keyname)]
    else:
        print('These are the contents' + str(contents))
        print('No key ' + str(KEY(keyname)) + ' added')


def findDeepestLock(content, depth):
    if isinstance(content, list):
        result = flatten([findDeepestLock(item, depth+1) for item in content])
        result = list(filter(None.__ne__, result))
        if result == []:
            result = None
        else:
            result = max(result, key=lambda tup: tup[1])
        return result
    elif isinstance(content, LOCK):
        contenttype = getContentType(content)
        if contenttype in [MIXED, ONLYLOCKS]:
            result = flatten([findDeepestLock(item, depth+1)
                             for item in content.getContent()])
            return list(filter(None.__ne__, result))
        elif contenttype == ONLYKEYS:
            return (content, depth)
        else:
            return None
    elif isinstance(content, KEY):
        return None
    elif isinstance(content, str):
        return None
    else:
        print('findDeepestLock The structure was unexpected ' + str(content))


def getContentType(lock):
    keys = 0
    locks = 0
    if isinstance(lock.getContent(), list):
        keys = len(lock.getContentKeys())
        locks = len(lock.getContentLocks())
    elif isinstance(lock.getContent(), LOCK):
        locks = 1
    elif isinstance(lock.getContent(), KEY):
        keys = 1

    if keys > 0 and locks > 0:
        return MIXED
    elif keys > 0:
        return ONLYKEYS
    elif locks > 0:
        return ONLYLOCKS
    else:
        return OTHER


def removeKey(keyname, content):
    result = []
    if isinstance(content, list):
        for item in content:
            if isinstance(item, LOCK):
                result.append(removeKey(keyname, item))
            elif isinstance(item, KEY):
                if item.getName() != keyname:
                    result.append(removeKey(keyname, item))
                else:
                    print('Ignoring item: ' + str(item))
            elif isinstance(item, str):
                result.append(removeKey(keyname, item))
    elif isinstance(content, LOCK):
        content.contents = removeKey(keyname, content.getContent())
        result = content
    else:
        result = content
    return result


def isKeyPresent(keyname, content):
    if isinstance(content, list):
        return any([isKeyPresent(keyname, item) for item in content])
    elif isinstance(content, LOCK):
        return isKeyPresent(keyname, content.getContent())
    elif isinstance(content, KEY):
        if content.getName() == keyname:
            return True
        else:
            return False
    elif isinstance(content, str):
        return False
    else:
        print('isKeyPresent The structure was unexpected ' + str(content))


def getRandomItems(dungeon, maxitemlimit):
    # Max of how many items we are going to put behind a lock
    maxrandomitems = random.randint(1, min(len(dungeon.getContent()),
                                           maxitemlimit))

    result = random.sample(dungeon.getContent(), maxrandomitems)

    return result


def choseLoopExistence(normlock, splock, dlock, multilock, looplock,
                       lockoptions):
    if lockoptions != []:
        result = random.choice([0, 1 if looplock else 0])
    else:
        print('At this point is no longer possible to add the looplock.')
        print('It will be skipped.')
        result = -2
    return result


def generateRandomStep(dungeon, normlockmax, splockmax, dlockmax, d2lockmax,
                       multilockmax, statelockmax, looplockmax):
    selecteditems = getRandomItems(dungeon, 3)
    dkey = None
    d2key = None
    if dlockmax == 1:
        print('Generating the dungeon item1.')
        dkey = 1
    else:
        dkey = 0

    if d2lockmax == 1:
        print('Generating the dungeon item2.')
        d2key = 1
    else:
        d2key = 0
    # From which locks can we choose.
    lockoptions = generateLockOptions(normlockmax, splockmax,
                                      dlockmax + dkey, d2lockmax + d2key,
                                      multilockmax, statelockmax,
                                      selecteditems)
    loopresult = choseLoopExistence(normlockmax, splockmax, dlockmax,
                                    multilockmax, looplockmax, lockoptions)
    # If there are no locksoptiosn we skip this iteration
    if len(lockoptions) != 0:
        # select one of the key options to use
        locktype = random.choice(lockoptions)
        if isinstance(locktype, list):
            print('The lock type is list')
            (loopresult, multilockresult) = addMultiLock(dungeon,
                                                         locktype,
                                                         selecteditems,
                                                         loopresult)
            looplockmax -= loopresult
            multilockmax -= multilockresult
        elif locktype == SPECIALKEY:
            print('The lock type is special')
            loopresult, splockresult = addSpecialLock(dungeon,
                                                      genSPKeyName(splockmax),
                                                      selecteditems,
                                                      loopresult)
            looplockmax -= loopresult
            splockmax -= splockresult
        elif locktype == DKEY:
            print('The lock type is dungeon item')
            (loopresult, dlockresult) = addDungeonItemLock(dungeon,
                                                           DKEY,
                                                           selecteditems,
                                                           dkey,
                                                           loopresult)
            looplockmax -= loopresult
            dlockmax -= dlockresult
        elif locktype == D2KEY:
            print('The lock type is dungeon2 item')
            (loopresult, d2lockresult) = addDungeonItemLock2(dungeon,
                                                             D2KEY,
                                                             selecteditems,
                                                             d2key,
                                                             loopresult)
            looplockmax -= loopresult
            d2lockmax -= d2lockresult
        elif locktype == SMALLKEY:
            print('The lock type is normal')
            (loopresult, normlockresult) = addNormalLock(dungeon,
                                                         selecteditems,
                                                         loopresult)
            looplockmax -= loopresult
            normlockmax -= normlockresult
        elif locktype == STATEKEY:
            print('The lock type is state')
            (loopresult,
             statelockresult) = addStateLock(dungeon,
                                             genStateKeyName(statelockmax),
                                             selecteditems,
                                             loopresult)
            looplockmax -= loopresult
            print(statelockresult)
            statelockmax -= statelockresult

    print(dungeon)
    print()
    return [normlockmax, splockmax, dlockmax, d2lockmax, multilockmax,
            statelockmax, looplockmax if loopresult != -2 else loopresult]


def generateRandomDungeon(dungeon, normalkeymax, specialkeymax, dungeonitemmax,
                          dungeon2itemmax, multiplekeymax, statekeymax,
                          loopmax):
    print('Starting to generate a dungeon ...')
    print('Using the following parameters:')
    print('-Normal Locks: ........' + str(normalkeymax))
    print('-Special Locks: .......' + str(specialkeymax))
    print('-Dungeon Item Locks: ..' + str(dungeonitemmax))
    print('-Dungeon2 Item Locks: .' + str(dungeon2itemmax))
    print('-Multi Locks: .........' + str(multiplekeymax))
    print('-State Locks: .........' + str(statekeymax))
    print('-Looping Lock: ........' + str(loopmax))
    print()

    (normalkey,
     specialkey,
     dungeonitem,
     dungeon2item,
     multiplekey,
     statekey,
     loop) = [normalkeymax,
              specialkeymax,
              dungeonitemmax + (1 if dungeonitemmax > 0 else 0),
              dungeon2itemmax + (1 if dungeon2itemmax > 0 else 0),
              multiplekeymax,
              statekeymax,
              loopmax]
    while ((normalkey + specialkey + multiplekey + statekey + loop) > 0
            or dungeonitem > 0 or
            dungeon2item > 0):

        (normalkey,
         specialkey,
         dungeonitem,
         dungeon2item,
         multiplekey,
         statekey,
         loop) = generateRandomStep(dungeon,
                                    normalkey,
                                    specialkey,
                                    dungeonitem,
                                    dungeon2item,
                                    multiplekey,
                                    statekey,
                                    loop)

        print('Normal Locks left:' + str(normalkey) +
              ' Special Locks left:' + str(specialkey) +
              ' Dungeon Item Locks left:' + str(dungeonitem) +
              ' Dungeon2 Item Locks left:' + str(dungeon2item) +
              ' Multi Locks left:' + str(multiplekey) +
              ' State Locks left:' + str(statekey) +
              ' Looping Lock left:' + str(loop))
        print()
    dungeon = DUNGEON(STARTKEY(), STARTLOCK(dungeon.getContent()))
    print(dungeon)
    return dungeon
