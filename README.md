# Lock-Key-Reward-Generator

This library allows the user to write a dungeon using the KEY/LOCK notation and the program will find a solution to it or a complete solution testing every possible combination of steps that allows it to be completed. If it finds a path that ends with keys not being used, locks not beind opened or not finding the exit it will report it.

There is another group of functions that allow the creation of a dungeon randomly. You only need to specify the number of keys that will be included in the dungeon.

Finally there is a function that will allow to generate a graphic when provided with a dungeon. The user can specify if it will be a 'png', 'svg' or 'both'.

For the solving and generation of dungeons the program only needs a basic installation of Python3

For the graphic side it needs only the drawSvg dependency which can be installed like this:

```
$ pip3 install drawSvg
```

This project is still a work in progress.

It was inspired on the works from Mark in the Boss Keys series on Youtube. Specifically this [video](https://www.youtube.com/watch?v=KEVJXqV7XMc).


Here are a few examples of the software:

### Solver:

```
----------Dragonroost------------
---- Path 1 ----
Action 1: []
Current keys: [⚷Start]
Dungeon Status: [!Start->[⚷K, !K->[⚷K, !K->[⚷K, !K->[⚷K, !K->[(⚷I), ⚷B, !I->!B->R]]]]]]

Action 2: ['Use key Start', 'Get key K']
Current keys: [⚷K]
Dungeon Status: [!K->[⚷K, !K->[⚷K, !K->[⚷K, !K->[(⚷I), ⚷B, !I->!B->R]]]]]

Action 3: ['Use key K', 'Get key K']
Current keys: [⚷K]
Dungeon Status: [!K->[⚷K, !K->[⚷K, !K->[(⚷I), ⚷B, !I->!B->R]]]]

Action 4: ['Use key K', 'Get key K']
Current keys: [⚷K]
Dungeon Status: [!K->[⚷K, !K->[(⚷I), ⚷B, !I->!B->R]]]

Action 5: ['Use key K', 'Get key K']
Current keys: [⚷K]
Dungeon Status: [!K->[(⚷I), ⚷B, !I->!B->R]]

Action 6: ['Use key K', 'Get key I', 'Get key B']
Current keys: [(⚷I), ⚷B]
Dungeon Status: [!I->!B->R]

Action 7: ['Use key I']
Current keys: [(⚷I), ⚷B]
Dungeon Status: [!B->R]

Action 8: ['Use key B', 'Get reward R']
Current keys: [(⚷I)]
Dungeon Status: []

There are 0 paths that ended without rewards
Key/Lock count: 13
Depth: 7
Complexity: 8
--------------------------------------------------------------------------------
```

### Generator:
Note: Any dungeon that this program generates is yours to take. That includes the graph of that dungeon. If you decide to do that it would be nice to be mentioned wherever you decide to use it.

```
--------- Random dungeon ----------
Starting to generate a dungeon ...
Using the following parameters:
-Normal Locks: ........3
-Special Locks: .......1
-Dungeon Item Locks: ..1
-Dungeon2 Item Locks: .0
-Multi Locks: .........0
-State Locks: .........0
-Looping Lock: ........0

The lock type is normal
Current contents: [⚷k, !k->[⚷BOSSKEY, !BOSSKEY->END]]

Normal Locks left:2 Special Locks left:1 Dungeon Item Locks left:1 Dungeon2 Item Locks left:1 Multi Locks left:0 State Locks left:0 Looping Lock left:0

The lock type is normal
Current contents: [⚷k, ⚷k, !k->!k->[⚷BOSSKEY, !BOSSKEY->END]]

Normal Locks left:1 Special Locks left:1 Dungeon Item Locks left:1 Dungeon2 Item Locks left:1 Multi Locks left:0 State Locks left:0 Looping Lock left:0

The lock type is normal
Current contents: [⚷k, ⚷k, !k->[!k->!k->[⚷BOSSKEY, !BOSSKEY->END], ⚷k]]

Normal Locks left:0 Special Locks left:1 Dungeon Item Locks left:1 Dungeon2 Item Locks left:1 Multi Locks left:0 State Locks left:0 Looping Lock left:0

The lock type is dungeon item
Current contents: [⚷k, !k->[!k->!k->[⚷BOSSKEY, !BOSSKEY->END], ⚷k], !DKEY->⚷k]

Normal Locks left:0 Special Locks left:1 Dungeon Item Locks left:0 Dungeon2 Item Locks left:0 Multi Locks left:0 State Locks left:0 Looping Lock left:0

Generating the dungeon item.
The lock type is dungeon item
Current contents: [!k->[!k->!k->[⚷BOSSKEY, !BOSSKEY->END], ⚷k], !DKEY->⚷k, ⚷k, (⚷DKEY)]

Normal Locks left:0 Special Locks left:1 Dungeon Item Locks left:-1 Dungeon2 Item Locks left:-1 Multi Locks left:0 State Locks left:0 Looping Lock left:0

The lock type is special
Current contents: [!k->[!k->!k->[⚷BOSSKEY, !BOSSKEY->END], ⚷k], !DKEY->⚷k, (⚷DKEY), ⚷A, !A->⚷k]

Normal Locks left:0 Special Locks left:0 Dungeon Item Locks left:-1 Dungeon2 Item Locks left:-1 Multi Locks left:0 State Locks left:0 Looping Lock left:0

Current keys: [⚷Start]
Current contents: [!Start->[!k->[!k->!k->[⚷BOSSKEY, !BOSSKEY->END], ⚷k], !DKEY->⚷k, (⚷DKEY), ⚷A, !A->⚷k]]

```


### Graphics:
## Dragonroost
![Dragonroost](examples/dragonroost.png)
## Tower of the gods
![Tower of the gods](examples/towerofthegods.png)
## Wind Temple
![Wind Temple](examples/windtemple.png)
## Random Dungeon
![Randomly Generated Dungeon](examples/generateddungeon.png)
